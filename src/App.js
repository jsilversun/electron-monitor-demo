import React from 'react';
import {ipcRenderer} from 'electron';
import 'bootstrap/dist/css/bootstrap.css';

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {test: []};
        this.handleProcessData = this.handleProcessData.bind(this);
        console.log('mounted');
    }

    componentDidMount() {
        ipcRenderer.on('PROCESSES_DATA', this.handleProcessData);
        ipcRenderer.send('COMPONENT_MOUNTED')
    }

    componentWillUnmount() {
        ipcRenderer.removeListener('PROCESSES_DATA', this.handleProcessData);
    }

    handleProcessData(e, arg) {
        const test = this.state.test;
        this.setState({test: [...test, arg]});
    };

    render() {
        const data = this.state.test;
        let headers = [];
        if (data.length) {
            headers = Object.keys(data[0]);
        }
        console.log(headers);
        return (
            <div className="table-responsive">
                <table className="table">
                    <tr>
                        {headers.map(header => <th  className="col-xs-2 d-inline-block text-truncate" scope="col">{header}</th>)}
                    </tr>
                    {data.map(row => <tr>
                        {Object.values(row).map(col => <td className="col-xs-2 d-inline-block text-truncate" >{col}</td>)}
                    </tr>)}
                </table>
            </div>
        );
    }

}

export default App;