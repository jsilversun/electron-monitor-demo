const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const ipcMain = electron.ipcMain;
const activeWin = require('active-win');
const path = require('path');
const isDev = require('electron-is-dev');

let mainWindow;

function createWindow() {
    mainWindow = new BrowserWindow({
        width: 1400, height: 900, webPreferences: {
            devTools: false
        }
    },);
    mainWindow.loadURL(isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, '../build/index.html')}`);
    if (isDev) {
        // Open the DevTools.
        //BrowserWindow.addDevToolsExtension('<location to your react chrome extension>');
        mainWindow.webContents.openDevTools();
    }
    const func = (event, arg) => {
        setInterval(async () => {
            try {
                const processData = await activeWin();
                const formattedObject = {
                    title: processData.title,
                    app: processData.owner.name,
                    bundleId: processData.owner.bundleId
                };
                mainWindow.send('PROCESSES_DATA', formattedObject);
            } catch (e) {
                console.error(e);
            }
        }, 5000);
    };
    ipcMain.on('COMPONENT_MOUNTED', func);
    mainWindow.on('closed', () => mainWindow = null);
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (mainWindow === null) {
        createWindow();
    }
});